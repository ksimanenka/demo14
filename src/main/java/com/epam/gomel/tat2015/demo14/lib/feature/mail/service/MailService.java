package com.epam.gomel.tat2015.demo14.lib.feature.mail.service;

import com.epam.gomel.tat2015.demo14.lib.feature.common.Account;
import com.epam.gomel.tat2015.demo14.lib.feature.mail.Letter;
import com.epam.gomel.tat2015.demo14.framework.report.Logger;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class MailService {

    public void sendLetter(Account account, Letter letter) {
        Logger.debug("Send letter: " + letter.toString());
        if(letter.containsAttach()) {
            letter.getAttachment().getAbsolutePath();
        }
    }

    public boolean isLetterExistInInbox(Account account, Letter letter) {
        Logger.debug("Check is letter exists in inbox: " + letter.toString());
        return false;
    }

}
