package com.epam.gomel.tat2015.demo14.tests.mail;

import com.epam.gomel.tat2015.demo14.lib.feature.common.Account;
import com.epam.gomel.tat2015.demo14.lib.feature.common.AccountBuilder;
import com.epam.gomel.tat2015.demo14.lib.feature.mail.service.MailLoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class MailMissedCredentialLoginTest {

    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS = "";
    private static final String EXPECTED_ERROR_MESSAGE_IN_CASE_NON_EXISTED_ACCOUNT = "";
    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getAccountWithWrongPass();

    @Test(description = "Check expected error message in case of wrong password")
    public void checkErrorMessageWrongPassword() {
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin(account);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_WRONG_PASS, "");
    }

    @Test(description = "Check expected error in case of non existed account")
    public void checkErrorMessageNonExistedAccount() {
        account = AccountBuilder.getNonExistedAccount();
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin(account);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE_IN_CASE_NON_EXISTED_ACCOUNT, "");
    }

}
