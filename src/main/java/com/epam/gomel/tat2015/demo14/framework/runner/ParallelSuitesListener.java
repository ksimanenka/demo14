package com.epam.gomel.tat2015.demo14.framework.runner;

import com.epam.gomel.tat2015.demo14.framework.config.GlobalConfig;
import com.epam.gomel.tat2015.demo14.framework.report.Logger;
import org.testng.ISuite;
import org.testng.ISuiteListener;

/**
 * @author Aleh_Vasilyeu
 */
public class ParallelSuitesListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        Logger.info("SUITE_START : " + suite.getName());
        suite.getXmlSuite().setParallel(GlobalConfig.config().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.config().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {
        Logger.info("SUITE_FINISH : " + suite.getName());
    }
}
