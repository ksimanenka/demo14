package com.epam.gomel.tat2015.demo14.lib.feature.mail.screen;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class MailboxMenu {

    // Locators

    public InboxPage openInbox() {
        return new InboxPage();
    }

    public SentPage openSent() {
        return new SentPage();
    }

    public DeletedPage openDeleted() {
        return new DeletedPage();
    }

    public SpamPage openSpam() {
        return new SpamPage();
    }

    public DraftsPage openDrafts() {
        return new DraftsPage();
    }

}
