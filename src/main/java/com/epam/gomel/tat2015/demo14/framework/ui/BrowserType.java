package com.epam.gomel.tat2015.demo14.framework.ui;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public enum BrowserType {

    FIREFOX("firefox"),
    CHROME("chrome");

    String name;

    BrowserType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
