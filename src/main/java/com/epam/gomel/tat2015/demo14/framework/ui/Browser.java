package com.epam.gomel.tat2015.demo14.framework.ui;

import com.epam.gomel.tat2015.demo14.framework.ui.exceptions.BrowserException;
import com.epam.gomel.tat2015.demo14.framework.report.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.epam.gomel.tat2015.demo14.framework.config.GlobalConfig.config;

public class Browser implements WrapsDriver {

    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 7;
    public static final long WEBDRIVER_IMPLICIT_TIME_OUT = 4;
    private WebDriver driver;

    private static Map<Thread, Browser> instances = Collections.synchronizedMap(new HashMap<Thread, Browser>());

    private Browser() {
    }

    public static synchronized Browser rise() {
        if (browserFotThread() != null) {
            browserFotThread().quit();
        }
        Browser browser = new Browser();
        browser.createDriver();
        instances.put(Thread.currentThread(), browser);
        return browser;
    }

    public static synchronized Browser current() {
        if (isBrowserOpened()) {
            return browserFotThread();
        }
        return null;
    }

    private static Browser browserFotThread() {
        return instances.get(Thread.currentThread());
    }

    private synchronized static boolean isBrowserOpened() {
        Browser browser = browserFotThread();
        return browser != null && browser.getWrappedDriver() != null;
    }

    public void quit() {
        Logger.debug("Stop browser");
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception ignore) {
        } finally {
            instances.remove(Thread.currentThread());
        }
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    private void createDriver() {
        switch (config().getBrowserType()) {
            default:
            case FIREFOX:
                Logger.debug("Start firefox browser");
                this.driver = isRemote() ? remoteFirefox() : localFirefoxDriver();
                break;
            case CHROME:
                Logger.debug("Start chrome browser");
                this.driver = isRemote() ? remoteChrome() : localChromeDriver();
                break;
        }
        this.driver.manage().timeouts().implicitlyWait(WEBDRIVER_IMPLICIT_TIME_OUT, TimeUnit.SECONDS);
//        this.driver.manage().window().maximize();
    }

    private boolean isRemote() {
        String seleniumHub = config().getSeleniumHub();
        return seleniumHub != null && seleniumHub != "";
    }

    private WebDriver localFirefoxDriver() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", "tmp/download");
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        profile.setPreference("browser.startup.homepage_override.mstone", "ignore");
        profile.setPreference("browser.startup.page", "0");
        return new FirefoxDriver(profile);
    }

    private WebDriver localChromeDriver() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> content_setting = new HashMap<>();
        content_setting.put("automatic_downloads", 1);
        content_setting.put("popups", 1);
        Map<String, Object> prefs = new HashMap<>();
        prefs.put("download.default_directory", "tmp/download");
        prefs.put("download.prompt_for_download", "false");
        prefs.put("profile.default_content_setting_values", content_setting);
        options.setExperimentalOption("prefs", prefs);
        options.addArguments("--disable-web-security");
        options.addArguments("--allow-running-insecure-content");
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        return new ChromeDriver(cap);
    }

    private WebDriver remoteFirefox() {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.dir", "tmp/download");
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        profile.setPreference("browser.startup.homepage_override.mstone", "ignore");
        profile.setPreference("browser.startup.page", "0");
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        return createRemote(capabilities);
    }

    private WebDriver remoteChrome() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        return createRemote(capabilities);
    }

    private RemoteWebDriver createRemote(DesiredCapabilities capabilities) {
        RemoteWebDriver driver;
        try {
            driver = new RemoteWebDriver(new URL(config().getSeleniumHub()), capabilities);
        } catch (MalformedURLException e) {
            throw new BrowserException("URL to remote has not valid format : " + e.getMessage(), e);
        }
        return driver;
    }

    public void open(String url) {
        Logger.debug("Open page : " + url);
        getWrappedDriver().get(url);
    }

    public void waitForAppear(By locator) {
        Logger.debug("Wait for element: " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void writeText(By locator, String text) {
        Logger.debug("Write text to : " + locator + " text = '" + text + "'");
        screenshot();
        getWrappedDriver().findElement(locator).sendKeys(text);
    }

    public void click(By locator) {
        Logger.debug("Click on element: " + locator);
        screenshot();
        getWrappedDriver().findElement(locator).click();
        screenshot();
    }

    public String getText(By locator) {
        Logger.debug("Get text of element: " + locator);
        return getWrappedDriver().findElement(locator).getText();
    }

    public static void screenshot() {
        WebDriver driver = current().getWrappedDriver();
        if (isBrowserOpened()) {
            try {
                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                File screenshotFile = new File(screenshotName());
                FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
                Logger.save(screenshotFile.getName());
            } catch (Exception e) {
                Logger.error("Failed to write screenshot: " + e.getMessage(), e);
            }
        }
    }

    private static String screenshotName() {
        return config().getResultDir() + File.separator + System.nanoTime() + ".png";
    }

}
